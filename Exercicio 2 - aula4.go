package main

import (
	"fmt"
)

// 2. Usando função faça um algoritmo que converte uma temperatura em Celsius para Fahrenheit ou vice-versa.

func main() {
	
  // Variaveis
  var temperatura float64
  var escala string
  var resultado float64

  // Entrada
  fmt.Println( "Digite a escala da temperatura \n(Digite C - Celsius e F - Fahrenheit): ")
  fmt.Scan(&escala)
  fmt.Println("Digite a temperatura: ")
  fmt.Scan(&temperatura)

  if escala == "C" || escala == "c" {
    resultado = calculaFahrenheit(temperatura)
    fmt.Printf("Temperatura em Fahrenheit: %.2f\n", resultado)
  } else if escala == "F" || escala == "f"{
    resultado = calculaCelsius(temperatura)
    fmt.Printf("Temperatura em Celsius: %.2f\n", resultado)
  } else {
    fmt.Print("Dados Inválidos")
  }

}

func calculaCelsius(valor float64) float64 {
  return (valor - 32) * 5/ 9
}

func calculaFahrenheit(valor float64) float64 {
  return (valor * 9/5) + 32
}