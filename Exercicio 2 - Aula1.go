package main

import (
	"fmt"
)

// 2. Faça um algoritmo que calcule a área de um retângulo com base e altura informados pelo usuário

func main() {
	// Definição de variaveis
  var base float64
  var altura float64

  // Dados
  fmt.Println("Digite a base do retangulo:")
  fmt.Scanln(&base)

  fmt.Println("Digite a altura do retangulo: ")
  fmt.Scanln(&altura)

  // Calculo
  var area = (base * altura)

  // Exibir a área calculada 
  
  fmt.Printf("A área do retângulo é: %.2f\n", area) 

}
