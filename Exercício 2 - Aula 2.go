package main

// Escreva um algoritmo que solicita ao usuário dois operandos e um código de operação (1 - Soma, 2 - Subtração, 3 - Divisão ou 4 - Multiplicação) e realiza a operação correspondente sobre os operandos fornecidos pelo usuário:

import (
	"fmt"
)

func main() {
	var operando1 , operando2 float64
  var codigo int

  fmt.Println("Digite o primeiro operando: ")
  fmt.Scan(&operando1)
  fmt.Println("Digite o segundo operando: ")
  fmt.Scan(&operando2)
  fmt.Println("Digite o código (1 - Soma, 2 - Subtração, 3 - Divisão ou 4 - Multiplicação) da operação: ")
  fmt.Scan(&codigo)

  if codigo == 1 {
    fmt.Println("Soma: ", operando1 + operando2)
  } else if codigo == 2 {
    fmt.Println("Subtração: ", operando1 - operando2)
  } else if codigo == 3 {
    fmt.Println("Divisão: ", operando1 / operando2)
  } else if codigo == 4 {
    fmt.Println("Multiplicação: ", operando1 * operando2)
  } else {
    fmt.Println ("Operação inválida")
  }
  
}
