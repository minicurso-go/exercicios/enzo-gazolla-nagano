package main
import "fmt"

// 3. Faça um algoritmo que imprima a tabuada de multiplicação de 1 a 10 para um número fornecido pelo usuário.

func main() {
  
  var numero int
  
  fmt.Println("Digite o numero da tabuada:")
  fmt.Scan(&numero)
  
  for i := 1; i <= 10; i++ {
    resultado := numero * i
    fmt.Printf("%d x %d = %d\n", numero, i, resultado)
  }
 
}