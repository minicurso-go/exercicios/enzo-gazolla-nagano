package main

import (
	"fmt"
)

// 4. Usando função faça um algoritmo que calcula o fatorial de um número, ou seja, o produto de todos os números inteiros de 1 até aquele número.

func main() {
  var numero int
  fmt.Print("Digite um número: ")
  fmt.Scanln(&numero)

  resultado := fatorial(numero)
  fmt.Printf("O fatorial de %d é %d\n", numero, resultado)
}

func fatorial(n int) int {
  if n == 0 {
    return 1
  }

  produto := 1
  for i := 1; i <= n; i++ {
    produto *= i
  }
  return produto
}

// Pesquisei na internet e falei com o meu pai, seria melhor usar função recursiva, mas como ainda não vimos. Então não usei

