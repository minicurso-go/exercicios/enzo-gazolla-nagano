package main

import (
	"fmt"
)

// 3. Faça um algoritmo que calcule o volume de uma caixa com base, altura e profundidade informados pelo usuário.

func main() {
    var base, altura, profundidade float64

    // Solicitar as dimensões da caixa ao usuário
    fmt.Println("Digite o valor da base da caixa:")
    fmt.Scanln(&base)

    fmt.Println("Digite o valor da altura da caixa:")
    fmt.Scanln(&altura)

    fmt.Println("Digite o valor da profundidade da caixa:")
    fmt.Scanln(&profundidade)

    // Calcular o volume da caixa
    var volume = base * altura * profundidade

    // Exibir o resultado
    fmt.Printf("O volume da caixa é: %.2f\n", volume)
}
	
