package main

import "fmt"

// 5. Faça um algoritmo que leia o valor do dólar em reais e um valor em dólares, e converta esse valor para reais.

func main() {
    var taxaDolar float64
    var valorDolar float64

    // Solicita que o usuário insira a taxa de conversão do dólar para reais
    fmt.Print("Digite a taxa de conversão do dólar para reais: ")
    fmt.Scanln(&taxaDolar)

    // Solicita que o usuário insira o valor em dólares a ser convertido
    fmt.Print("Digite o valor em dólares: ")
    fmt.Scanln(&valorDolar)

    // Converte o valor em dólares para reais
    valorEmReais := valorDolar * taxaDolar

    // Exibe o resultado da conversão
    fmt.Printf("%.2f dólares equivalem a %.2f reais.\n", valorDolar, valorEmReais)
  
}
