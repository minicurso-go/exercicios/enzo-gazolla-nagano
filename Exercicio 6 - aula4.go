package main

import (
	"fmt"
)

// ¨6. Usando função faça um algoritmo que calcula a potência de um número, elevando-o a uma determinada potência.

func main() {
 
 // Variáveis
 var potencia int
 var valor int
  
  // Dados
  fmt.Println("Digite o valor: ")
  fmt.Scan(&valor)
  fmt.Println ("Digite a potencia: ")
  fmt.Scan (&potencia)
  
  // Chamada de funcao
  fmt.Println (valor, "elevado a", potencia, "é igual a: ", potenciacao (valor, potencia))
}

func potenciacao (valor int, potencia int) int {
  resultado := 1
  for i := 0; i < potencia; i++ {
    resultado *= valor
  }
  return resultado
}