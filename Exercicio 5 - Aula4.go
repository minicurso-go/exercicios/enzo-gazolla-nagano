package main

import (
	"fmt"
)

// 5. Usando função faça um algoritmo que procura por um determinado elemento em uma lista, verificando cada elemento da lista até encontrá-lo.

func main() {
  var alvo int
  lista := []int{1,2,3,4,5,6,7,8,9,10} 
  fmt.Println("Digite números de 1 a 10: ")
  fmt.Scan(&alvo)
  resultado := busca(alvo, lista)
  if resultado {
    fmt.Println ("Valor encontrado")
  } else {
    fmt.Println ("Valor não encontrado")
  }
}

func busca(alvo int, lista []int) bool {
  for i := 0; i < len(lista); i++ {
    if lista[i] == alvo {
      return true
    } 
  } 
  return false
}

// Pesquisei e falei com o meu pai sobre como fazer a busca e tiver que usar array, se tiver outro jeito de fazer, me avise.
