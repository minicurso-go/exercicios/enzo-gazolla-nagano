package main

import (
	"fmt"
)

// 3. Usando função faça um algoritmo que calcula a média de uma lista de números.

func main() {
	lista := []float64{1, 2, 3, 4, 5}
  media := calcularMedia (lista)
  fmt.Printf ("A média da lista é: %.2f\n", media)
}

func calcularMedia (lista []float64) float64 {
  total := 0.0
  for _, valor := range lista {
    total += valor
  }
  return total / float64(len(lista))
}

// Tive que usar consulta a internet por não termos visto ainda a função len() e listas (array). Se houver outro jeito sem prescisar disso, por favor me avise.