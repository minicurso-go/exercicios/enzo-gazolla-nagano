package main
import "fmt"

// 1. Usando função faça um algoritmo simples que pede dois números como entrada e retorna a soma desses dois números.

func soma(n1 int, n2 int) int {
    return n1 + n2
}

func main() {
  var n1, n2 int
  
  fmt.Println("Digite o primeiro numero: ")
  fmt.Scan(&n1)
  fmt.Println("Digite o segundo numero: ")
  fmt.Scan(&n2)
  
  s := soma(n1 , n2)
  
  fmt.Println("Valor da Soma: ", s)
}