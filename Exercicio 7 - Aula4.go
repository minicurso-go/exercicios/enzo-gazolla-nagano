package main

import (
	"fmt"
)

// 7. Usando função faça um algoritmo que verifica se um número é primo, ou seja, se é divisível apenas por 1 e por ele mesmo.

func main() {
  var num int
  fmt.Println("Digite um número: ")
  fmt.Scan(&num)

  result := ePrimo(num)
   if result == true {
     fmt.Println("O número é primo")
   } else {
     fmt.Println("O número não é primo")
   }
  
}

func ePrimo(num int) bool {
  if num == 1 { return false }
  if num == 2 { return true }
  if num == 3 { return true }
  if num % 2 == 0 || num % 3 == 0 {
    return false
  } else {
    return true
  }
}