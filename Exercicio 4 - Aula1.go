package main

import (
  "fmt"
)

// 4. Faça um algoritmo que calcule a média aritmética de quatro números informados pelo usuário.

func main() {
  var num1, num2, num3, num4 float64

  // Solicitar ao usuário que insira os números
  fmt.Println("Digite o primeiro número:")
  fmt.Scanln(&num1)
  
  fmt.Println("Digite o segundo número:")
  fmt.Scanln(&num2)
  
  fmt.Println("Digite o terceiro número:")
  fmt.Scanln(&num3)
  
  fmt.Println("Digite o quarto número:")
  fmt.Scanln(&num4)

  // Calcular a média aritmética
  var media = (num1 + num2 + num3 + num4) / 4

  // Exibir a média aritmética
 
  fmt.Printf("A média aritmética é: %.2f\n", media)
  
}