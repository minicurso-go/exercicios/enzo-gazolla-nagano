package main

import (
  "fmt"
  )

// 6. Faça um algoritmo que leia vários números inteiros e mostre o maior deles. A leitura deve ser interrompida quando for lido o valor zero.

func main() {
	var numero = -1
  var maior = 0
  
  for (numero != 0) {
    fmt.Print("Digite um número inteiro (0 para sair): ")
    fmt.Scan(&numero)
    if numero > maior {
       maior = numero
    } 
  } 
  fmt.Println("O maior número é: ", maior)
}