package main

/* Fazer um programa que leia três valores com ponto flutuante de dupla precisão: A, B e C. Em seguida, calcule e mostre:

a) a área do triângulo retângulo que tem A por base e C por altura.
b) a área do círculo de raio C. (pi = 3.14159).
c) a área do trapézio que tem A e B por bases e C por altura. 
d) a área do quadrado que tem lado B.
e) a área do retângulo que tem lados A e B.*/

import (
	"fmt"
)

func main() {
  var A, B, C float64

  fmt.Println("Digite o valor de A: ")
  fmt.Scanf("%f", &A)
  fmt.Println("Digite o valor de B: ")
  fmt.Scanf ("%f", &B)
  fmt.Println("Digite o valor de C: ")
  fmt.Scanf ("%f", &C)

   triangulo := (A * C) / 2
   circulo := 3.14159 * C * C
   trapezio := ((A + B) * C )/ 2
   quadrado := B * B
   retangulo := A * B

  fmt.Println("Triangulo: ", triangulo)
  fmt.Println("Circulo: ", circulo)
  fmt.Println("Trapezio: ", trapezio)
  fmt.Println ( "Quadrado: ", quadrado)
  fmt.Println ("Retangulo: ", retangulo)
  
}