package main
import "fmt"

// 1. Faça um algoritmo que imprima os múltiplos de 3 de 0 a 30.

func main() {
    for i:= 0; i <= 30; i +=3 {
        fmt.Println(i)
    }
}