package main

import "fmt"

// ¨5. Faça um algoritmo que leia um número inteiro positivo e mostre todos os seus divisores.

func main() {
	var numero int
  
  fmt.Print("Digite um número inteiro positivo: ")
  fmt.Scan(&numero)

  if numero <= 0 {
    fmt.Println("O número deve ser positivo.")
      return
  }
  
  fmt.Printf("Divisores de %d: %d\n", numero, numero)
  for i := 1; i <= numero; i++ {
    if numero % i == 0 {
      fmt.Println (i)
    }
  }

}
