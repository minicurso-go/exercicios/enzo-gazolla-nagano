package main

/* Fazer um programa para ler quatro valores inteiros A, B, C e D. A seguir, calcule e mostre a diferença do produto de C e D segundo a fórmula: DIFERENÇA = (A * B - C * D).

Entrada:                 Saída:
5                             Diferença = -26
6
7
8*/

import (
	"fmt"
)

func main() {
	var A, B, C, D int

  fmt.Println("Digite o valor de A: ")
  fmt.Scan(&A)
  fmt.Println("Digite o valor de B: ")
  fmt.Scan (&B)
  fmt.Println("Digite o valor de C: ")
  fmt.Scan (&C)
  fmt.Println("Digite o valor de D: ")
  fmt.Scan (&D)

  Diferenca := (A * B - C * D)
  fmt.Printf("Diferença = %d\n", Diferenca)
}
